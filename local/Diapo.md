---
title: RAPPORT DE PROJET CRÉATION RÉSEAU D'ENTREPRISE
author:
- Dylan LEBBRECHT
- Bilal DALHOUMI
- Fatoumata DANSOKO
date: 15 Janvier 2021
theme:
- Copenhagen
top-level-division: part
---

| Projet                            |
|-----------------------------------|
| Création d'un Réseau d'Entreprise |
| Superviseur                       |
| Yvan Peter                        |


# Sommaire

- **Présentation du Projet** 

- **Les composants du projet**

- **Suivi du Projet**

- **Préparation par une maquette Cisco Packet Tracer**

- **Réseau IP**

- **Pare-Feu**

- **Anti-Spam**

- **Serveur**

- **PC Clients**

# Présentation du Projet

- **Présentation du Projet**

- Les composants du projet

- Suivi du Projet

- Préparation par une maquette Cisco Packet Tracer

- Réseau IP

- Pare-Feu

- Anti-Spam

- Serveur

- PC Clients

# Présentation du Projet

Le projet consiste à créer un réseau d'entreprise permettant de mettre
en pratique les connaissances acquises lors du premier semestre.

![](img/diagrammeRzo.png)

# Présentation du Projet

Le réseau contient 3 VLANs qui sont en inter-VLAN :

- VLAN Comptabilité : 192.168.1.0/24
- VLAN Production : 192.168.2.0/24
- VLAN Serveur : 192.168.3.0/24

Les 2 réseaux sont reliés via un réseau public : 180.1.1.252/30

# Les composants du projet

- Présentation du Projet 

- **Les composants du projet**

- Suivi du Projet

- Préparation par une maquette Cisco Packet Tracer

- Réseau IP

- Pare-Feu

- Anti-Spam

- Serveur

- PC Clients

# Les composants du projet

![](img/puzzle.jpg){ width=20% }

Voici les composants que nous avons utilisé pour ce projet :

-   1 Routeur
-   1 Switch
-   6 PCs (4 Utilisateurs, un Pare-Feu et un Serveur)
-   Des câbles Ethernet
-   Serveur Web (Apache2) 
-   Serveur Mail (Postfix) 
-   Serveur DHCP (Isc-dhcp-server)
-   Serveur DNS (Bind9) 
- 	Pare-Feu (Iptables)

# Suivi du Projet

- Présentation du Projet 

- Les composants du projet

- **Suivi du Projet**

- Préparation par une maquette Cisco Packet Tracer

- Réseau IP

- Pare-Feu

- Anti-Spam

- Serveur

- PC Clients

# Outils pour la création du rapport

- Git
- Markdown
- Pandoc
- LaTeX
- Draw.io

# Git

![](img/git.png){ width=30% }

![](img/pageGitlab.png)

# Markdown

![](img/logo-markdown.pdf){ width=30% }

<https://daringfireball.net/projects/markdown>

Un langage de balisage l'écriture simple du rapport

# Pandoc

![](img/pandoc.png){ width=30% }

<https://pandoc.org>

Pour la conversion des fichiers, voici la nôtre.
```
pandoc RapportDeProjet.md -o RapportDeProjet.pdf
```

# LaTeX

![](img/latex.jpg){ width=30% }

<https://www.latex-project.org>

Pour la conversion vers le pdf car pandoc ne peut pas le faire seul.

# Draw.io


![](img/draw.io-logo.png){ width=30% }

<https://app.diagrams.net/>

Site en ligne permettant de créer des diagrammes.

# Exemple de Rapport Hebdomadaire

## Semaine 1

- Création de Machines Virtuelles pour faire des tests
- Analyse du cahier des charges
- Recherches sur les différents services disponibles

# Tâches

| Tâches | Prédécesseurs | Intitulé de la tâche             |
|--------|---------------|----------------------------------|
| A      | /             | Adressage IP                     |
| B      | /             | Architecture du réseau           |
| C      | A,B           | Configuration du Switch          |
| D      | A,B           | Configuration du Routeur         |
| E      | A,B           | Installation des Postes Clients  |
| F      | A,B           | Installation du Serveur          |
| G      | A,B           | Mise en place du Pare-Feu        |
| H      | F             | Configuration du DHCP            |
| I      | F             | Configuration du DNS             |
| J      | F             | Configuration du WEB             |
| K      | F             | Configuration du mail            |

# Tâches

| Tâches | Prédécesseurs | Intitulé de la tâche             |
|--------|---------------|----------------------------------|
| L      | F,G           | Configuration du SSH             |
| M      | C             | Configuration des VLAN           |
| N      | A,B,D         | Mise en œuvre de l'infra commune |
| O      | D             | Configuration du NAT             |
| P      | D             | Configuration du Routage         |
| Q      | /             | Rédaction du rapport             |
| R      | Q             | Création du diaporama            |

# Préparation par une maquette Cisco Packet Tracer

- Présentation du Projet 

- Les composants du projet

- Suivi du Projet

- **Préparation par une maquette Cisco Packet Tracer**

- Réseau IP

- Pare-Feu

- Anti-Spam

- Serveur

- PC Clients

# Préparation par une maquette Cisco Packet Tracer 

Cela a permis de maquetter le réseau avant de pouvoir manipuler physiquement le matériel et également pendant le confinement.

![](img/rzoPT.png)

Deux réseaux comme dans le schéma présenté précédemment.

# Préparation par une maquette Cisco Packet Tracer 

PC1-GRP1 vers Serveur-GRP1 :
```
C:\>ping proj-cgir.com
Ping statistics for 192.168.3.253:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
```

PC1-GRP1 vers PC1-GRP2 :
```
C:\>ping 192.168.4.2
Ping statistics for 192.168.4.2:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
```

# Réseau IP

- Présentation du Projet

- Les composants du projet

- Suivi du Projet

- Préparation par une maquette Cisco Packet Tracer

- **Réseau IP**

- Pare-Feu

- Anti-Spam

- Serveur

- PC Clients

# Switch

![](img/sw.jpg){ width=40% }

Pour améliorer la qualité de service du réseau, ce dernier a été séparé en 3 VLANs :

-   **VLAN Comptabilité** : PC1, PC2 192.168.1.0 - 192.168.1.255/24
-   **VLAN Production** : PC3, PC4 192.168.2.0 - 192.168.2.255/24
-   **VLAN Serveur** : Serveur 192.168.3.0 - 192.168.3.255/24

# Switch

Ainsi que leurs adresses IP pour y accéder en SSH :  

- **VLAN Comptabilité** : 192.168.1.252
- **VLAN Production**   : 192.168.2.252
- **VLAN Serveur**      : 192.168.3.252

# Routeur

| Adresse IP Privée | Adresse IP Publique |
|-------------------|---------------------|
| 192.168.3.253     | 180.1.1.254         |

- **Gig0/0.1 (VLAN 2)** : 192.168.1.254/24
- **Gig0/0.2 (VLAN 3)** : 192.168.2.254/24
- **Gig0/0.3 (VLAN 4)** : 192.168.3.254/24
- **Gig0/1**            : 180.1.1.254/30

# Routeur

*ip helper-address* permet de renseigner le serveur DHCP distant

```
interface GigabitEthernet0/0.1
 encapsulation dot1Q 2
 ip address 192.168.1.254 255.255.255.0
 ip helper-address 192.168.3.253
!
interface GigabitEthernet0/0.2
 encapsulation dot1Q 3
 ip address 192.168.2.254 255.255.255.0
 ip helper-address 192.168.3.253
```

# Routeur

Accès à distance en SSH

```
username CiscoR secret 5 $1$mERr$cYYFPHBAtGJMMdZZinb5i1
ip ssh version 2
ip ssh authentication-retries 5
ip ssh time-out 60
ip domain-name proj-cgir.com
!
line vty 0 4
 login local
 transport input ssh
```

# Pare-Feu

- Présentation du Projet 

- Les composants du projet

- Suivi du Projet

- Préparation par une maquette Cisco Packet Tracer

- Réseau IP

- **Pare-Feu**

- Anti-Spam

- Serveur

- PC Clients

# Pare-Feu

IPTables : 

- Licence Libre GPL
- Complet 
- Déjà installé de base sur Debian

```
chmod +x /etc/init.d/parefeu
update-rc.d parefeu defaults
```

# Pare-Feu

| N° | Permission | Protocole | Source        | Destination | Port |
|----|------------|-----------|---------------|-------------|------|
| 1  | DENY       |           | Any           | Any         |      |
| 2  | ALLOW      |           | localhost     | localhost   |      |
| 3  | ALLOW      | ICMP      | Any           | Any         |      |
| 4  | ALLOW      | TCP       | Any           | Any         | 4168 |
| 5  | ALLOW      | TCP       | Any           | Any         | 53   |
| 6  | ALLOW      | UDP       | Any           | Any         | 53   |
| 7  | ALLOW      | TCP       | Any           | Any         | 80   |
| 8  | ALLOW      | TCP       | Any           | Any         | 443  |
| 9  | ALLOW      | TCP       | Any           | Any         | 25   |
| 10 | ALLOW      | TCP       | Any           | Any         | 110  |
| 11 | ALLOW      | TCP       | Any           | Any         | 143  |
| 12 | ALLOW      | TCP       | Any           | Any         | 123  |

# Anti-spam

- Présentation du Projet 

- Les composants du projet

- Suivi du Projet

- Préparation par une maquette Cisco Packet Tracer

- Réseau IP

- Pare-Feu

- **Anti-Spam**

- Serveur

- PC Clients

# Anti-spam

Anti-spam via fail2ban :

- Éviter que quelqu'un rentre dans le serveur sans autorisation
- Éviter de flood les logs

```
bantime  = 10m
findtime  = 10m
maxretry = 5
[sshd]
port    = 4168
```

# Serveur

- Présentation du Projet 

- Les composants du projet

- Suivi du Projet

- Préparation par une maquette Cisco Packet Tracer

- Réseau IP

- Pare-Feu

- Anti-Spam

- **Serveur**

- PC Clients

# SSH

```
Port 4168
PermitRootLogin no
PasswordAuthentication no
```
Authentification asymétrique + passphrase  
Clé publique du client : */home/debianserv/.ssh/authorized_keys*.

# DHCP

*isc-dhcp-server* est parmi les plus répandus et a quand même pas mal d'expérience car il a été créé en 1999.

|        | Adresse Minimum | Adresse Maximum | Passerelle    |
|--------|-----------------|-----------------|---------------|
| VLAN 2 | 192.168.1.1     | 192.168.1.100   | 192.168.1.254 |
| VLAN 3 | 192.168.2.1     | 192.168.2.100   | 192.168.2.254 |

MàJ auto du DNS, serveur autoritaire sur le réseau : *192.168.3.253*

Clé RNDC partagée avec DNS pour désigner les machines par des noms.

# DHCP


```
key "rndc-key" {
        algorithm hmac-md5;
        secret "clé secrète rndc-key";
}
zone proj-cgir.com {
        primary 192.168.3.253;
        key "rndc-key";
}
zone 3.168.192.in-addr.arpa {
        primary 192.168.3.253;
        key "rndc-key";
}
```


# DHCP

Voici le résultat côté client :
```
Dec  3 13:12:03 debianClient1 dhclient[777]: DHCPDISCOVER
on enp0s3 to 255.255.255.255 port 67 interval 10

Dec  3 13:12:03 debianClient1 dhclient[777]: DHCPOFFER
of 192.168.1.99 from 192.168.3.253

Dec  3 13:12:03 debianClient1 dhclient[777]: DHCPREQUEST
for 192.168.1.99 on enp0s3 to 255.255.255.255 port 67

Dec  3 13:12:03 debianClient1 dhclient[777]: DHCPACK
of 192.168.1.99 from 192.168.3.253

Dec  3 13:12:03 debianClient1 avahi-daemon[325]: Joining
mDNS multicast group on interface enp0s3.IPv4 with address 192.168.1.99.
```

# DNS

*bind9* est l'implémentation la plus répandue et est libre.

Voici la configuration du *named.conf.local*
```
zone "proj-cgir.com" {
        type master;
        file "/var/lib/bind/db.direct";
        allow-update { key rndc-key; };
};

zone "3.168.192.in-addr.arpa" IN {
        type master;
        file "/var/lib/bind/db.reverse";
        allow-update { key rndc-key; };
};

```

# DNS

```
 key "rndc-key" {
        algorithm hmac-md5;
        secret "clé secrète rndc-key";
 };

 controls {
        inet 127.0.0.1 port 953
                allow { 127.0.0.1; } keys { "rndc-key"; };
 };
```

# DNS Enregistrements

| Nom de domaine               | Type d'enregistrement    | Adresse IP    |
|------------------------------|--------------------------|---------------|
| ns.proj-cgir.com.            | Translation Nom <-> IP   | 192.168.3.253 |
| www.proj-cgir.com.           | Translation Nom <-> IP   | 192.168.3.253 |
| debianClient1.proj-cgir.com. | Translation Nom <-> IP   | 192.168.1.81  |
| proj-cgir.com.               | Serveur faisant autorité |               |

| Nom de domaine               | Serveur faisant autorité |
|------------------------------|--------------------------|
| ns.proj-cgir.com.            |                          |
| www.proj-cgir.com.           |                          |
| debianClient1.proj-cgir.com. |                          |
| proj-cgir.com.               | ns.proj-cgir.com.        |


# Mail

<http://www.postfix.org/>

![](img/postfix.png){ width=30% }

- Permettre de communiquer efficacement 
- **postfix** est bien répandu, fiable, libre et a de l'expérience.

# Serveur Web

<https://apache.org/>

![](img/apache.png){ width=30% }

**apache2** :  

- Open-Source
- Créé en 1995 donc communauté conséquente

# Serveur Web

Page Web accessible via **proj-cgir.com**

![](img/site.jpg){ width=90% }

# PC Clients

- Présentation du Projet 

- Les composants du projet

- Suivi du Projet

- Préparation par une maquette Cisco Packet Tracer

- Réseau IP

- Pare-Feu

- Anti-Spam

- Serveur

- **PC Clients**

# PC Clients

![](img/pcClients.jpg){ width=30% }

- Linux Debian avec l'IDE Mate.
- Gratuit et open-source.
- Rien à configurer et une interface graphique pour les non-informaticiens.
- 4 PC pour tester connexion inter VLAN intra VLAN