---
title: RAPPORT DE PROJET CRÉATION RÉSEAU D'ENTREPRISE
author:
- Dylan LEBBRECHT
- Bilal DALHOUMI
- Fatoumata DANSOKO
date: \today
geometry:
- margin=1in
header-includes:
- \usepackage{fancyhdr}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{\includegraphics[width=2cm]{img/logo-iut-lille1.pdf}}
- \fancyhead[CO,CE]{Projet de Création de Réseau d'entreprise}
- \fancyhead[RO,RE]{\includegraphics[width=4cm]{img/logo-cgir.pdf}}
- \fancyfoot[CO,CE]{Page \thepage}
---

![](img/logo-iut-lille1.pdf)
![](img/logo-cgir.pdf)

| Projet                            |
|-----------------------------------|
| Création d'un Réseau d'Entreprise |
| Superviseur                       |
| Yvan Peter                        |


\tableofcontents

# Remerciements

Nous tenons à d'abord remercier l'ensemble des personnes nous ayant aidés dans la réalisation de ce projet :

-   **Yvan Peter** : De nous avoir supervisé, aidé, suivi et consacré de son temps

-   **Bruno Beaufils** : De nous avoir fourni des fichiers pour Git et le pour le rapport

-   **L'IUT A de Lille** : Pour nous avoir fourni le matériel et les
    connaissances nécessaires afin de concrétiser ce projet.

# Présentation du Projet

## Introduction

Le projet consiste à créer un réseau d'entreprise permettant de mettre en pratique les connaissances acquises lors du premier semestre. Dans le cahier des charges plusieurs choses nous ont été demandées :

-	Le réseau comporte une partie publique accessible de l’extérieur (i.e., les autres groupes) et une
partie privée pour les machines utilisateurs. (NAT)
-	Chaque groupe hébergera un serveur Web accessible aux autres groupes.
-	Les utilisateurs pourront s’échanger des mails y compris d’un groupe à l’autre. (Serveur Mail)
-	La partie privée sera sécurisé par un PC sous linux. (Pare-Feu)
-	Les adresses de la partie privée seront en adressage privé et ne devront pas être visible à l’extérieur.
-	Les machines de la partie privée appartiennent soit au département comptabilité soit au dépar-
tement production. Les ﬂux de ces diﬀérents départements devront être séparés pour assurer la
qualité de service du réseau. Toutefois, une communication entre les machines des deux départe-
ments doit rester possible. (VLANs + Routage inter-VLAN)
-	De manière à faciliter la vie de l’administrateur et des utilisateurs, les machines utilisateur seront
conﬁgurées dynamiquement. Les diﬀérentes machines utilisateurs ainsi que les serveurs pourront
être désignés par un nom.  (DHCP + DNS)  

Si l’ensemble de ces éléments a pu être mis en œuvre avant la ﬁn du projet, on pourra mettre en
place les éléments suivants :  

-	Fournir l’ensemble des services en IPv4 et en IPv6
-	Oﬀrir un accès sécurisé depuis un réseau extérieur (VPN).
-	Oﬀrir aux utilisateurs un accès Wiﬁ. Le ﬂux correspondant sera séparé des autres ﬂux et permettra
uniquement le ﬂux HTTP.

Chaque groupe désignera un membre chargé de la mise en œuvre de l’infrastructure commune permettant de relier les diﬀérentes entreprises.  
Donc, nous allons vous présenter le déroulement de notre projet ainsi que les solutions mises en place et pourquoi nous les avons faites.

\newpage

# Suivi du projet

## Description des outils de suivi du projet et de création du rapport

### Git

![](img/git.png){ width=30% }

Afin de faire des sauvegardes régulières et de voir l'évolution du projet au fil des mois, nous avons fait du versionning, pour cela nous
avons utilisé GitLab.  

Gitlab nous a permis de tous ensemble avancés sur le projet depuis nos postes respectifs. Il fut très utile durant les périodes de confinemment.
Nous avons créer un Git sur la plateforme Gitlab.  

Après nous être connecté sur la plateforme Gitlab de la faculté (https://gitlab.univ-lille.fr/) nous avons créé un groupe Gitlab. Les groupes du GitLab permettent de regrouper des projets. Cela nous a permis de créer et d'organiser notre projet et de le partager avec les membres uniquements. 
 

Le projet étant créer nous avons ensuite chacun installer Git Bash ( https://gitforwindows.org/ ), ce logiciel nous a permis d'envoyer sur le projet nos ajouts et modifications. La configuration du Git est passé par une initialisation assez classique. 

| Commande                                         | Intitulé de la commande                                     |
|--------------------------------------------------|-------------------------------------------------------------|
| git clone "lien_du_projet"                       | Création d'un clone du dépôt distant sur la machine locale. |
| git add "fichier"                                | Ajout d'un fichier au dépot local                           |
| git commit -m "Message expliquant le changement" | Envoie du fichier/changement au dépot                       |
| git push                                         | Chargement du dépot local au dépot distant                  | 

Cela étant fait nous avons pu démarrer le projet. 

On retrouve sur Gitlab diverses sections, tel que la page activité, la page dépot qui récapitule les données se trouvant sur le dépot, les membres, la taille du dépot, le nombre de git (changement au dépot)


![](img/pageGitlab.png)

### Markdown  

![](img/md.png){ width=30% }

<https://daringfireball.net/projects/markdown>

Un langage de balisage pour une écriture simple du rapport.
D'habitude nous utilisons des logiciels tels que LibreOffice ou Word pour créer un rapport mais cette fois-ci nous utilisons markdown et pandoc, ce qui rend la chose plus rapide et simple à créer tout en rendant un résultat visuel plutôt satisfaisant.

### Pandoc  

![](img/pandoc.png){ width=30% }

<https://pandoc.org>

Pour la conversion des fichiers, voici la nôtre.
```
pandoc RapportDeProjet.md -o RapportDeProjet.pdf
```

### LaTeX  


![](img/latex.jpg){ width=30% }

<https://www.latex-project.org>

Pour la conversion vers le pdf car pandoc ne peut pas le faire seul.

### Draw.io


![](img/draw.io-logo.png){ width=30% }

<https://app.diagrams.net/>

Afin de créer le schéma du réseau, nous avons utilisé draw.io, c'est un outil en ligne permettant de créer des diagrammes, des schémas, organigrammes, etc. C'est un outil gratuit et il n'y a pas besoin de se connecter. Pour ma part (Dylan Lebbrecht), l'ayant utilisé durant mon projet de BTS, il m'a été très utile. 

## Rapport Hebdomadaire

### Semaine 1

- Création de Machines Virtuelles pour faire des tests
- Analyse du cahier des charges
- Recherches sur les différents services disponibles

### Semaine 2

- Commencement du projet sur Packet Tracer car la salle n'était pas disponible
- Adressage IP du réseau (PT)
- Architecture du réseau en fonction des besoins (PT)

### Semaine 3

- Création du DHCP sur le Routeur (PT)
- Création de VLANs dans le Switch (PT)
- Création du mail dans le serveur (PT)
- Création du DNS dans le serveur (PT)
- Création du NAT pour le serveur (PT)
- Création du réseau du Groupe 2 en parallèle avec celui du Groupe 1 pour faire des tests inter-réseaux (PT)

### Semaine 4

- Installation isc-dhcp-server
- Installation apache
- Importation et adaptation des configs Routeur et Switch créées sur PT vers le matériel réel

### Semaine 5

- Création du Repository Git
- Créations de VM Clientes sur Debian (avec interfaces graphiques)

### Semaine 6

- Ré-adressage IP
- Test du fonctionnement du DHCP inter-VLANs
- Tentative d'installation du DNS (bind9)

### Semaine 7

- Continuation du Packet Tracer (Confinement)
- Installation de ufw sur une VM
- Réorganisation du Git
- DHCP inter-VLANs fonctionnel

### Semaine 8

- Documentation sur la création d'un réseau virtuel sur VirtualBox
- Configuration de IPTables
- Documentation sur fail2ban + installation
- Installation de LaTeX pour convertir de markdown vers pdf avec pandoc

### Semaine 9

- Documentation sur fail2ban ainsi que SSH sur le réseau
- Configuration du SSH du serveur + clés

### Semaine 10

- SSH sur Routeur et Switch
- Fail2ban configuré
- Implémentation et amélioration du DNS créé par Fatoumata

### Semaine 11

- DNS mis à jour automatiquement, machines désignées par des noms

### Semaine 12

- Avancement sur le mail
- Création et commencement de la diapo

### Semaine 13

- Problèmes avec le mail

### Semaine 14

- Envois de mail fonctionnel
- Préparation de l'oral

### Semaine 15

- Préparation de l'oral

## Tâches

| Tâches | Prédécesseurs | Intitulé de la tâche             |
|--------|---------------|----------------------------------|
| A      | /             | Adressage IP                     |
| B      | /             | Architecture du réseau           |
| C      | A,B           | Configuration du Switch          |
| D      | A,B           | Configuration du Routeur         |
| E      | A,B           | Installation des Postes Clients  |
| F      | A,B           | Installation du Serveur          |
| G      | A,B           | Mise en place du Pare-Feu        |
| H      | F             | Configuration du DHCP            |
| I      | F             | Configuration du DNS             |
| J      | F             | Configuration du WEB             |
| K      | F             | Configuration du mail            |
| L      | F,G           | Configuration du SSH             |
| M      | C             | Configuration des VLAN           |
| N      | A,B,D         | Mise en œuvre de l'infra commune |
| O      | D             | Configuration du NAT             |
| P      | D             | Configuration du Routage         |
| Q      | /             | Rédaction du rapport             |
| R      | Q             | Création du diaporama            |

# Schéma du réseau

![](img/diagrammeRzo.png)


Voici le schéma des 2 réseaux, les PCs de la partie privée sont en adressage privée, les interfaces extérieures des Routeurs elles sont publiques.  
Juste à l'entrée du réseau côté intérieur se trouve un Pare-Feu sous Linux.  
Le serveur contient un service web, mail, DHCP, DNS.  
Le switch contient 3 VLANs :

- VLAN Comptabilité
- VLAN Production
- VLAN Serveur

Ces 3 VLANs peuvent communiquer entre eux grâce au routage inter-VLAN.  
Ensuite les 2 réseaux sont reliés via un réseau public.

# Préparation par une maquette Cisco Packet Tracer 

Au départ du projet nous n'avions pas accès à la salle nous permettant de manipuler le matériel destiné à mettre en place le réseau. Pour palier à cela nous avons créé le réseau sur Cisco Packet Tracer afin de faire nos tests de notre côté.


![](img/rzoPT.png)

Étant donné qu'il y a deux groupes différents ayant chacun le même
projet mais que chacun des serveurs doit être accessible à l'autre
groupe, nous avons re-créer le réseau de l'autre groupe afin de
confirmer nos tests.

Plus tard se trouve la description en détails du réseau.

Mais tout de même le réseau sur Packet Tracer fonctionne (il n'y a pas le pare-feu cependant), les PC peuvent communiquer entre eux.

PC1-GRP1 vers Serveur-GRP1 :
```
C:\>ping proj-cgir.com
Ping statistics for 192.168.3.253:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
```

PC1-GRP1 vers PC1-GRP2 :
```
C:\>ping 192.168.4.2
Ping statistics for 192.168.4.2:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
```

# Les composants du projet

![](img/puzzle.jpg){ width=30% }

Voici les composants que nous avons utilisé pour ce projet :

-   1 Routeur
-   1 Switch
-   6 PCs (4 Utilisateurs, un Pare-Feu et un Serveur)
-   Des câbles Ethernet
-   Serveur Web (Apache2) 
-   Serveur Mail (Postfix + Dovecot) 
-   Messagerie Mail (Thunderbird)
-   Serveur DHCP (Isc-dhcp-server)
-   Serveur DNS (Bind9) 
- 	Pare-Feu (Iptables)

# Pourquoi tel service et pas un autre ?

En effet, nous étions plutôt libre dans nos choix, que ça soit sur l'OS
du serveur ou des utilisateurs ou encore sur le serveur web, mais nous
avons décidé de faire ces choix pour ce projet :

Nous avons favorisé l'utilisation de solutions libres et gratuites du à leurs perforances prouvées et leurs documentations disponible importante.

- C'est le cas notamment pour les systèmes d'exploitation des Ordinateurs, ils sont tous sur Linux, car la sécurité est une priorité, en effet étant donné que les droits par exemple sont mieux gérés sur Linux favorise ce choix.
- Pour ce qui est du pare-feu nous avons choisi Iptables car il est déjà présent de base sur Debian et qu'il est assez complet.
- Pour le serveur Web nous avons choisi apache2 pour sa communauté.
- Pour ce qui est du DNS, DHCP et Mail nous avons choisi bind9, isc-dhcp-server et postfix, ils sont plutôt répandus et ont pas mal d'expérience, ce qui fait que l'on peut se fier à ces derniers

# Réseau IP

## Switch

![](img/sw.jpg){ width=30% }

Nous allons nous concentrer sur la partie 'Groupe 1'.

Pour améliorer la qualité de service du réseau, ce dernier a été séparé en 3 VLANs :

-   **VLAN Comptabilité** : PC1, PC2 192.168.1.0 - 192.168.1.255/24
-   **VLAN Production** : PC3, PC4 192.168.2.0 - 192.168.2.255/24
-   **VLAN Serveur** : Serveur 192.168.3.0 - 192.168.3.255/24

Attribution des VLANs aux ports utilisés. Il y a deux ports qui utilisent le VLAN 2 (Comptabilité), deux autres qui utilisent le VLAN 3 (Production)
```
interface FastEthernet0/1
 switchport access vlan 2
 switchport mode access
!
interface FastEthernet0/2
 switchport access vlan 2
 switchport mode access
!
interface FastEthernet0/3
 switchport access vlan 3
 switchport mode access
!
interface FastEthernet0/4
 switchport access vlan 3
 switchport mode access
!
interface FastEthernet0/5
 switchport access vlan 4
 switchport mode access
interface FastEthernet0/15
 switchport mode trunk
```
Ainsi que leurs adresses IP pour y accéder en SSH :  

- **VLAN Comptabilité** : 192.168.1.252
- **VLAN Production**   : 192.168.2.252
- **VLAN Serveur**      : 192.168.3.252

```
interface Vlan2
 mac-address 0060.3e91.8401
 ip address 192.168.1.252 255.255.255.0
!
interface Vlan3
 mac-address 0060.3e91.8403
 ip address 192.168.2.252 255.255.255.0
!
interface Vlan4
 mac-address 0060.3e91.8402
 ip address 192.168.3.252 255.255.255.0
```

## Routeur

![](img/router.jpg){ width=30% }

Pour qu'il soit accessible depuis l'extérieur, un NAT sur le Routeur a été configuré permettant de relier les différentes entreprises.
Ci-dessous voici la configuration appliquée sur le Routeur pour traduire l'adresse IP privée du serveur vers une adresse publique
pour permettre à l'autre groupe d'y accèder.

| Adresse IP Privée | Adresse IP Publique |
|-------------------|---------------------|
| 192.168.3.253     | 180.1.1.254         |

- **Gig0/0.1 (VLAN 2)** : 192.168.1.254/24
- **Gig0/0.2 (VLAN 3)** : 192.168.2.254/24
- **Gig0/0.3 (VLAN 4)** : 192.168.3.254/24
- **Gig0/1**            : 180.1.1.254/30

```
interface GigabitEthernet0/0.3
 encapsulation dot1Q 4
 ip address 192.168.3.254 255.255.255.0
 ip nat inside
!
interface GigabitEthernet0/1
 ip address 180.1.1.254 255.255.255.252
 ip nat outside
 duplex auto
 speed auto
!
ip nat inside source static 192.168.6.253 180.1.1.254 
```

Le Serveur contient le service DHCP qui s'occupe d'attribuer automatiquement aux 4 PCs leurs adresses IP respectives. Comme il y a de l'inter-VLAN les 2 VLANs autres que le serveur n'avaient pas accès au service DHCP, pour cela une petite configuration a été appliquée sur les sous-interfaces.
```
interface GigabitEthernet0/0.1
 encapsulation dot1Q 2
 ip address 192.168.1.254 255.255.255.0
 ip helper-address 192.168.3.253
!
interface GigabitEthernet0/0.2
 encapsulation dot1Q 3
 ip address 192.168.2.254 255.255.255.0
 ip helper-address 192.168.3.253
```
*ip helper-address 192.168.3.253* permet de renseigner le serveur DHCP sur le Routeur pour que les VLANs Comptabilité et Production puissent bénificier de ce service.

### Sécurité

Afin de sécuriser le routeur mais tout de même pouvoir y accèder à distance, un SSH a été implanté ainsi qu'un chiffrement du mot de passe enable du routeur.

```
username CiscoR secret 5 $1$mERr$cYYFPHBAtGJMMdZZinb5i1
ip ssh version 2
ip ssh authentication-retries 5
ip ssh time-out 60
ip domain-name proj-cgir.com
!
line vty 0 4
 login local
 transport input ssh
```

### Relation entre le réseau du Groupe 1 et du Groupe 2

En effet, les deux réseaux devaient impérativement communiquer entre eux
```
ip route 192.168.4.0 255.255.255.0 180.1.1.253 
ip route 192.168.5.0 255.255.255.0 180.1.1.253 
ip route 192.168.6.0 255.255.255.0 180.1.1.253 
```

Les 3 réseaux sont mis en routage statique dans le routeur, ce qui permet à notre réseau d'accèder à l'autre réseau, la route passe par l'interface externe de l'autre routeur soit *180.1.1.253*.

### Importation des configurations sur le matériel réel

Avant le confinement, nous travaillions sur les machines réels. Étant donnée qu'au préalable nous avions fait les configurations sur Packet Tracer :
```
Router#copy tftp: running-config
Address or name of remote host []? 192.168.3.1
Source filename []? backup_cfg_for_my_router
Destination filename [running-config]?
Accessing tftp://192.168.3.1/backup_cfg_for_my_router...
Loading backup_cfg_for_router from 192.168.3.1 (via FastEthernet0/0): !
[OK - 1030 bytes]

1030 bytes copied in 9.612 secs (107 bytes/sec)
CE_2#
```
L'adresse IP utilisé correspond à l'adresse hôte car il y avait des serveurs TFTP dans les machines de la salle réseau.  
Et donc grâce à cela nous avons pu importer les configurations dans un switch et un routeur et avons pu tester quelques fonctionnalités comme le DHCP cependant pas le DHCP inter-VLAN.

# Pare-Feu

![](img/fw.jpg){ width=30% }

Afin de sécuriser le réseau comme demandé dans le cahier des charges, un pare-feu a été mis en place sur un PC sous Linux, le choix a été *IPTables* car il est sous licence libre GPL, au départ le choix a été porté sur ufw mais étant donné que IPTables est plus complet et est déjà installé de base, le choix est devenu IPTables.

## Règles du Pare-Feu

| N° | Permission | Protocole | Source        | Destination | Port |
|----|------------|-----------|---------------|-------------|------|
| 1  | DENY       |           | Any           | Any         |      |
| 2  | ALLOW      |           | localhost     | localhost   |      |
| 3  | ALLOW      | ICMP      | Any           | Any         |      |
| 4  | ALLOW      | TCP       | Any           | Any         | 4168 |
| 5  | ALLOW      | TCP       | Any           | Any         | 53   |
| 6  | ALLOW      | UDP       | Any           | Any         | 53   |
| 7  | ALLOW      | TCP       | Any           | Any         | 80   |
| 8  | ALLOW      | TCP       | Any           | Any         | 443  |
| 9  | ALLOW      | TCP       | Any           | Any         | 25   |
| 10 | ALLOW      | TCP       | Any           | Any         | 110  |
| 11 | ALLOW      | TCP       | Any           | Any         | 143  |
| 12 | ALLOW      | TCP       | Any           | Any         | 123  |

## Configuration d'IPTables

- Tout d'abord j'ai retiré la configuration de base

	```
	# iptables -t filter -F
	# iptables -t filter -X
	```

- Puis je suis parti sur une politique de refus total, les protocoles nécessaires seront autorisés ultérieurement

	```
	# iptables -P OUTPUT DROP
	# iptables -P INPUT DROP
	# iptables -P FORWARD DROP
	```

- Les connexions déjà établies restent établies, utile si on est connecté en SSH durant l'application des règles

	```
	# iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
	# iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
	```

- Permettre de pouvoir communiquer avec son adresse locale

	```
	# iptables -A INPUT -i lo -j ACCEPT
	# iptables -A OUTPUT -o lo -j ACCEPT 
	```

- Autoriser le ping

	```
	# iptables -t filter -A INPUT -p icmp -j ACCEPT
	# iptables -t filter -A OUTPUT -p icmp -j ACCEPT
	```

- Ensuite on laisse passer les protocoles que l'on a besoin, en entrée et en sortie pour la très grande majorité
	- SSH

		```
		# iptables -t filter -A INPUT -p tcp --dport 4168 -j ACCEPT
		# iptables -t filter -A OUTPUT -p tcp --dport 4168 -j ACCEPT
		```

	- DNS

		```
		# iptables -t filter -A INPUT -p tcp --dport 53 -j ACCEPT
		# iptables -t filter -A OUTPUT -p tcp --dport 53 -j ACCEPT
		# iptables -t filter -A INPUT -p udp --dport 53 -j ACCEPT
		# iptables -t filter -A OUTPUT -p udp --dport 53 -j ACCEPT
		```

	- NTP (pour l'heure)

		```
		# iptables -t filter -A INPUT -p udp --dport 123 -j ACCEPT
		# iptables -t filter -A OUTPUT -p udp --dport 123 -j ACCEPT
		```

	- HTTP

		```
		# iptables -t filter -A INPUT -p tcp --dport 80 -j ACCEPT
		# iptables -t filter -A OUTPUT -p tcp --dport 80 -j ACCEPT
		```

	- HTTPS

		```
		# iptables -t filter -A INPUT -p tcp --dport 443 -j ACCEPT
		# iptables -t filter -A OUTPUT -p tcp --dport 443 -j ACCEPT
		```

	- MAIL
		- SMTP

			```
			# iptables -t filter -A INPUT -p tcp --dport 25 -j ACCEPT
			# iptables -t filter -A OUTPUT -p tcp --dport 25 -j ACCEPT
			```

		- POP3

			```
			# iptables -t filter -A INPUT -p tcp --dport 110 -j ACCEPT
			# iptables -t filter -A OUTPUT -p tcp --dport 110 -j ACCEPT
			```

		- IMAP

			```
			# iptables -t filter -A INPUT -p tcp --dport 143 -j ACCEPT
			# iptables -t filter -A OUTPUT -p tcp --dport 143 -j ACCEPT
			```

Pour finir j'ai mis tout cela dans un script bash que j'ai placé dans */etc/init.d/* ensuite pour lui ajouter les droits d'éxecution et faire en sorte qu'il soit exécuté à chaque démarrage car la configuration est effacée à l'arrêt :
```
chmod +x /etc/init.d/parefeu
update-rc.d parefeu defaults
```

# Anti-spam

![](img/fail2ban.jpg){ width=30% }

Afin d'éviter que quelqu'un rentre dans le serveur alors qu'il n'est pas autorisé et aussi afin de ne pas flood les logs, un anti-spam a été mis en place via le service fail2ban.
```
bantime  = 10m
findtime  = 10m
maxretry = 5
[sshd]
port    = 4168
```

Le temps de bannissement est de 10 minutes et si sur une période de 10 minutes quelqu'un se trompe 5 fois d'authentification, alors il est banni, le port du ssh renseigné est le 4168.

# Serveur

![](img/serv.jpg){ width=30% }

## SSH

![](img/ssh.png){ width=30% }

Un SSH a été installé sur le serveur mais toutefois des modifications à la configuration ont été apportés :

```
Port 4168
PermitRootLogin no
PasswordAuthentication no
```
Le port du SSH qui par défaut est le *22* a été changé en *4168* afin de le rendre moins évident à trouver dans le cas où une personne non autorisé voudrait y accèder, l'autorisation de la connexion par root a également été interdite.
Pour ce qui est de l'authentification sur mot de passe qui a été retiré, c'est pour augmenter la sécurité car l'authentification se fait par clé (publique et privée) avec un passphrase, ce qui fait que uniquement les personnes possèdant les clés et le passphrase peuvent accèder au serveur en SSH.
Le client a donné sa clé publique au serveur qui l'a stocké dans */home/debianserv/.ssh/authorized_keys*.
Pour ce qui est de la génération des clés, il faut taper : 
```
ssh-keygen -t rsa -b 4096 -C "dylan.lebbrecht.etu@univ-lille.fr"
```
- *-t rsa* étant le chiffrement rsa
- *-b 4096* le chiffrement se fait sur 4096 bits
- *-C "dylan.lebbrecht.etu@univ-lille.fr"* est juste un commentaire dans lequel j'ai mis mon email

```
ssh-copy-id debianserv@192.168.3.253 -p 4168
```
Commande permettant de copier la clé publique vers le serveur, étant donné que le port a été changé au préalable dans la configuration, il faut le préciser lors de la connexion via le *-p 4168*.

```
ssh debianserv@192.168.3.253 -p 4168
Enter passphrase for key '~/.ssh/id_rsa':
```
Connexion au serveur.

## DHCP

<https://www.isc.org/dhcp/>

![](img/isc-dhcp-server.png){ width=30% }

Afin de respecter le cahier des charges un DHCP a été mis en place afin d'attribuer automatiquement aux PC clients des adresses IP, pour cela nous avons utilisé *isc-dhcp-server*.
Nous avons choisi ce dernier car il est parmi les plus répandus et a quand même pas mal d'expérience car il a été créé en 1999.
Voici la configuration appliquée :

|        | Réseau         | Adresse Minimum | Adresse Maximum | Passerelle    | Broadcast     |
|--------|----------------|-----------------|-----------------|---------------|---------------|
| VLAN 2 | 192.168.1.0/24 | 192.168.1.1     | 192.168.1.100   | 192.168.1.254 | 192.168.1.255 |
| VLAN 3 | 192.168.2.0/24 | 192.168.2.1     | 192.168.2.100   | 192.168.2.254 | 192.168.1.255 |

```
default-lease-time 600;
max-lease-time 7200;

ddns-update-style interim;

authoritative;
server-identifier 192.168.3.253;

# VLAN 2
subnet 192.168.1.0 netmask 255.255.255.0 {
  range 192.168.1.1 192.168.1.100;
  default-lease-time 600;
  option broadcast-address 192.168.1.255;
  option routers 192.168.1.254;
  option domain-name-servers ns.proj-cgir.com;
  option domain-name "proj-cgir.com";
  max-lease-time 7200;
}

# VLAN 3
subnet 192.168.2.0 netmask 255.255.255.0 {
  range 192.168.2.1 192.168.2.100;
  default-lease-time 600;
  option broadcast-address 192.168.2.255;
  option routers 192.168.2.254;
  option domain-name-servers ns.proj-cgir.com;
  option domain-name "proj-cgir.com";
  max-lease-time 7200;
}

key "rndc-key" {
        algorithm hmac-md5;
        secret "clé secrète rndc-key";
}
zone proj-cgir.com {
        primary 192.168.3.253;
        key "rndc-key";
}
zone 3.168.192.in-addr.arpa {
        primary 192.168.3.253;
        key "rndc-key";
}
```

Il y a une mise à jour automatique du serveur DNS, le serveur est autoritaire sur le réseau avec pour adresse IP *192.168.3.253*.

Il y a 2 pools d'adresses, celui de *VLAN 2 (Compta)* ainsi que celui du *VLAN 3 (Prod)* qui sont entres les adresses *192.168.X.1 et 192.168.X.100* avec comme passerelle par défaut les sous-interfaces des VLAN correspondants *192.168.X.254*, par défaut le temps d'attribution d'une adresse est de 10 minutes, mais cela peut aller jusqu'a 2 heures en cas de spécification du client.  

Ensuite on ajoute les zones du DNS ainsi que la clé rndc pour mettre à jour le DNS avec DHCP, ça peut notamment permettre de désigner les machines par des noms.

Voici le résultat côté client :
```
Dec  3 13:12:03 debianClient1 dhclient[777]: DHCPDISCOVER on enp0s3 to 255.255.255.255 port 67 interval 10
Dec  3 13:12:03 debianClient1 dhclient[777]: DHCPOFFER of 192.168.1.99 from 192.168.3.253
Dec  3 13:12:03 debianClient1 dhclient[777]: DHCPREQUEST for 192.168.1.99 on enp0s3 to 255.255.255.255 port 67
Dec  3 13:12:03 debianClient1 dhclient[777]: DHCPACK of 192.168.1.99 from 192.168.3.253
Dec  3 13:12:03 debianClient1 avahi-daemon[325]: Joining mDNS multicast group on interface enp0s3.IPv4 with address 192.168.1.99.
```

Malheuresement, nous n'avons pas vraiment pu tester le DHCP inter-réseau car nous n'avions pas accès au matériel, sur Cisco Packet Tracer, cela fonctionne, personnellement j'ai testé chez moi mais je n'ai pas de Switch, donc toutes les machines étaient dans le même réseau.

## DNS

<https://bind9.net/>

![](img/bind9.png){ width=30% }

Afin d'accèder au serveur web de façon plus simple pour les utilisteurs pas forcément à l'aise avec l'adressage IP mais aussi pour le service mail, un DNS a été mis en place, c'est un protocole quasiment obligatoire. Le service est *bind9*.
C'est l'implémentation la plus répandue et elle est également libre.

Voici la configuration du *named.conf.local*
```
zone "proj-cgir.com" {
        type master;
        file "/var/lib/bind/db.direct";
        allow-update { key rndc-key; };
};

zone "3.168.192.in-addr.arpa" IN {
        type master;
        file "/var/lib/bind/db.reverse";
        allow-update { key rndc-key; };
};

# Use with the following in named.conf, adjusting the allow list as needed:
 key "rndc-key" {
        algorithm hmac-md5;
        secret "clé secrète rndc-key";
 };

 controls {
        inet 127.0.0.1 port 953
                allow { 127.0.0.1; } keys { "rndc-key"; };
 };
# End of named.conf
```

Les fichiers de zones ont été placés dans */var/lib/bind/* parce qu'il y a des problèmes de droits dans */etc/bind*, ensuite une autorisation des mises à jour de rndc-key.
Ensuite la clé est renseignée, c'est la même que celle renseignée dans dhcpd.conf.

Voici les enregistrements du DNS :

| Nom de domaine               | Type d'enregistrement    | Adresse IP    | Serveur faisant autorité |
|------------------------------|--------------------------|---------------|--------------------------|
| ns.proj-cgir.com.            | Translation Nom <-> IP   | 192.168.3.253 |                          |
| www.proj-cgir.com.           | Translation Nom <-> IP   | 192.168.3.253 |                          |
| debianClient1.proj-cgir.com. | Translation Nom <-> IP   | 192.168.1.81  |                          |
| proj-cgir.com.               | Serveur faisant autorité |               | ns.proj-cgir.com.        |

Zone *db.direct* :
```
$ORIGIN .
$TTL 604800     ; 1 week
proj-cgir.com           IN SOA  ns.proj-cgir.com. root.proj-cgir.com. (
                                30         ; serial
                                604800     ; refresh (1 week)
                                86400      ; retry (1 day)
                                2419200    ; expire (4 weeks)
                                604800     ; minimum (1 week)
                                )
                        NS      ns.proj-cgir.com.
$ORIGIN proj-cgir.com.
$TTL 300        ; 5 minutes
debianClient1           A       192.168.1.81
                        TXT     "310b1663c25ac0d9f866a7ef5ec662499f"
$TTL 604800     ; 1 week
ns                      A       192.168.3.253
www                     A       192.168.3.253
mail                    A       192.168.3.253
```

Le serveur DNS et le serveur web possède donc la même adresse IP.
debianClient1 est un client à qui une adresse IP a été distribuée par le DHCP.

Zone *db.reverse* :
```
$ORIGIN .
$TTL 604800     ; 1 week
1.168.192.in-addr.arpa  IN SOA  ns.proj-cgir.com. root.proj-cgir.com. (
                                21         ; serial
                                604800     ; refresh (1 week)
                                86400      ; retry (1 day)
                                2419200    ; expire (4 weeks)
                                604800     ; minimum (1 week)
                                )
                        NS      ns.proj-cgir.com.
$ORIGIN 1.168.192.in-addr.arpa.
26                      PTR     ns.proj-cgir.com.
                        PTR     www.proj-cgir.com.
$TTL 300        ; 5 minutes
81                      PTR     debianClient1.proj-cgir.com.
```

### Soucis rencontrés

Il y avait des machines qui ne s'enregistraient pas automatiquement dans le DNS parce que la zone n'avait pas été mise à jour.

## Mail

<http://www.postfix.org/>

![](img/postfix.png){ width=30% }

Comme le cahier des charges le précisait, nous avons mis en place un serveur mail afin de permettre aux différents utilisateurs de communiquer efficacement, pour cela nous avons utilisé *postfix* comme serveur de transmission de courriers électroniques..
Il est bien répandu, fiable, libre et a de l'expérience.

Postfix est un serveur de messagerie STMP libre et répandu dans le domaine Linux. Postfix est assez simple dans son fonctionnement et sa configuration l'est aussi. Nous avons configuré le main.cf pour l'adapter à nos besoins. 

Aprés avoir télécharger et installer celui-ci dans le main.cf nous avons pu le configurer selon nos besoins dans le main.cf 

Vous trouverez ensuite les éléments de configuration les plus essentiel pour le service mail. 

myorigin = /etc/mailname - Ce paramètre permet de préciser le nom de domaine. Dans notre cas ce sera proj-cgir.com

mydestination = $myhostname localhost.$mydomain localhost $mydomain 
Permet de préciser pour quel domaine notre serveur postfix se chargera de transmettre les mails. 

myhostname = debianServ.proj-cgir.com
Précise le nom de la machine ainsi que le domaine. 

Après cela nous avons installé Dovecot, un serveur IMAP/POP3 permettant aux utilisateurs de consulter leurs mails. Nous avons configuré les boites aux lettres en format Maildir. 

Nous avons configuré les certificats pour le bon fonctionnement TLS/SSL.

Cela fait nous avons ensuite installé le client messagerie Thunderbird. Celui ci nous a permis de se connecter avec les comptes mails et à permis aux utilisateurs d'envoyer et de recevoir des mails. 

Thunderbird peut gérer plusieurs comptes de courrier électronique et de flux d'informations et prend en charge plusieurs identités au sein des comptes. Des fonctionnalités telles que la recherche rapide, un dossier de recherche sauvegardé et le regroupement des messages.

Il fut nécessaire d'enregistrer l'addresse mail dans le DNS. 

mail    A 			192.168.3.253
	 IN MX 			mail.proj-cgir.com.




## Serveur Web

<https://apache.org/>

![](img/apache.png){ width=30% }

Pour le serveur web nous avons décidé d'utiliser *apache* car il est open-source, il est plutôt ancien car il a été créé en 1995 ce qui fait qu'il possède une communauté conséquente, très utile en cas de problème pour obtenir rapidement de l'aide.

Voici la page web, à laquelle nous pouvons accèder via le DNS en rentrant *proj-cgir.com*.


![](img/site.jpg)

Voici la configuration du site web */etc/apache2/sites-available/000-default.conf*:
```
<VirtualHost *:80>
	DocumentRoot /var/www/webProjet
</VirtualHost>
```

Le fichier .html se trouve dans le dossier /var/www/webProjet.

# PC Clients


![](img/pcClients.jpg){ width=30% }


Maintenant, les principaux concernés, les clients, leurs Ordinateurs sont sur Linux Debian avec l'IDE Mate.
Nous avons choisi de les mettre sous Linux parce que c'est gratuit et open-source.
Étant donné qu'il y a un serveur DHCP, les utilisateurs n'ont rien à configurer et une interface graphique a été mise car les comptables par exemple ne sont pas forcément à l'aise en ligne de commandes.
Dans le cadre du projet, nous avons décidé de mettre 4 PC dans notre réseau afin de pouvoir tester la connexion inter-VLAN et intra-VLAN mais le changement du nombre de PC Clients est tout à fait possible.

\newpage

# Conclusion

Pour conclure, nous sommes fiers de ce que nous avons fait pour ce projet malgré les challenges auquels nous avons été confrontés comme le confinement ou encore l'indisponibilité de la salle. La mise en place de ce projet aura été très intéressante, il nous aura apporté à nous 3 des connaissances notables que ça soit dans l’élaboration d’un projet, sa réalisation, ou encore le savoir nécessaire.
Ces mois de travail nous aurons également appris à mieux nous organiser, à adopter l’esprit d’équipe.

Dylan Lebbrecht : "Personnellement ce projet m’a permis d’en apprendre plus sur le réseau, ses services, j'ai notamment trouvé intéressant le pare-feu car je n'en avais jamais fait auparavant mais ce que j'ai préféré faire c'est la configuration réseau ainsi que l'architecture" 

Bilal Dalhoumi : "Ce projet fut très instructif et gratifiant à mener. Nous avons eu quelques obstacles notamment dû au distanciel mais avec notre groupe nous avons su nous adapter aux changements qui nous furent imposés. Nous avons pu observer la construction d'un réseau sous différentes formes et cela fut très enrichissant bien que parfois complexe."

Fatoumata Dansoko : "Moi personnellement les parties qui m’ont le plus marqué sont le déploiement des services et la sécurité de l’infrastructure. J'ai appris la configuration du pare-feu ainsi que la configuration de certains services dans le routeur. Ce projet a été un plus moi car j’ai eu une idée sur le déploiement et la mise en place d’une infrastructure réseau."

Cependant le réseau n’a malheureusement pas pu se concrétiser à cause du coronavirus et du confinement, dû au fait que nous n'avons pas pu tester en réel tout cela.