!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Routeur-GRP1
!
!
!
enable secret 5 $1$mERr$k1CE1Jj.DFEAx1bLbHky40
!
!
!
!
!
!
ip cef
no ipv6 cef
!
!
!
username CiscoR secret 5 $1$mERr$cYYFPHBAtGJMMdZZinb5i1
!
!
license udi pid CISCO2901/K9 sn FTX152453KS-
!
!
!
!
!
!
!
!
!
ip ssh version 2
ip ssh authentication-retries 5
ip ssh time-out 60
ip domain-name proj-cgir.com
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface GigabitEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface GigabitEthernet0/0.1
 encapsulation dot1Q 2
 ip address 192.168.1.254 255.255.255.0
 ip helper-address 192.168.3.253
!
interface GigabitEthernet0/0.2
 encapsulation dot1Q 3
 ip address 192.168.2.254 255.255.255.0
 ip helper-address 192.168.3.253
!
interface GigabitEthernet0/0.3
 encapsulation dot1Q 4
 ip address 192.168.3.254 255.255.255.0
 ip nat inside
!
interface GigabitEthernet0/1
 ip address 180.1.1.254 255.255.255.252
 ip nat outside
 duplex auto
 speed auto
!
interface Serial0/3/0
 no ip address
 clock rate 64000
!
interface Serial0/3/1
 no ip address
 clock rate 2000000
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan2
 mac-address 00e0.a340.9101
 no ip address
 ip helper-address 192.168.3.253
!
interface Vlan3
 mac-address 00e0.a340.9102
 no ip address
 ip helper-address 192.168.3.253
!
router rip
!
ip nat inside source static 192.168.3.253 180.1.1.254 
ip classless
ip route 192.168.4.0 255.255.255.0 180.1.1.253 
ip route 192.168.5.0 255.255.255.0 180.1.1.253 
ip route 192.168.6.0 255.255.255.0 180.1.1.253 
!
ip flow-export version 9
!
!
ip access-list extended sl_def_acl
 deny tcp any any eq telnet
 deny tcp any any eq www
 deny tcp any any eq 22
 permit tcp any any eq 22
!
!
!
!
!
!
line con 0
!
line aux 0
 login
!
line vty 0 4
 login local
 transport input ssh
!
!
!
end

