# Bienvenue sur le Git du projet CGIR FDB

![image](local/img/diagrammeRzo.png)

Plan d'addressage IP : 

- Routeur :  
VLAN 2 192.168.1.254/24  Compta    
VLAN 3 192.168.2.254/24  Prod  
VLAN 4 192.168.3.254/24  Serveur  

Du aux réglementation COVID-19 nous avons du réaliser le projet en local et donc n'avons pas implémenté les VLAN cités au dessus. 

- Serveur Debian : 192.168.1.253
- DHCP : 192.168.1.1 -> 192.168.1.100 (ISC-DHCP-SERVER)
- Mail : mail.proj-cgir.com (Postfix / Dovecot / Thunderbird)
- DNS : bind9 
- Firewall : IPtables

# Projet CGIR
Le travail est dirigé par Yvan PETER et effectué par des étudiants de la CGIR dans le cadre de leur projet de mise en oeuvre au cours de l'année 2020 :

Dylan Lebbrecht   
Bilal Dalhoumi  
Fatoumata Dansoko

Le projet est géré en respectant quelques règles.

# Contenu

- [`bin`](bin) contient les scripts et programmes créées ainsi que les outils utilisés
- [`cfg`](cfg) contient les fichiers de configuration des services
- [`local`](local) contient les informations relatifs au déroulement du projet


# Droits
Copyright (C) 2020 Groupe FDB
Les scripts distribués ici le sont sous les termes de la licence général publique GPL, version 2, disponible dans le fichier GNU-GPL.
Les autres documents distribués ici sont mis à disposition selon les termes de la licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA), disponible dans le fichier CC-BY-NC-SA.
